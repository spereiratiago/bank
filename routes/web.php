<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CustomerController@index');
Route::resource('customer', 'CustomerController');
Route::get('customer/{account}/deposit', 'AccountController@depositView')->name('account.depositview');
Route::get('customer/{account}/widraw', 'AccountController@widrawView')->name('account.widrawview');;
Route::post('customer/{account}/deposit', 'AccountController@deposit')->name('account.deposit');
Route::post('customer/{account}/widraw', 'AccountController@widraw')->name('account.widraw');;