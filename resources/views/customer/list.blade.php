@extends('master.base')

@section('main')
<div class="row">
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      {{ session()->get('success') }}  
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
</div>
<div class="col-sm-12">
    <h3>Customers List</h3>
    <div>
    <a style="margin: 19px;" href="{{ route('customer.create')}}" class="btn btn-primary">New Costumer</a>
    </div> 
    @if(isset($customers))   
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Name</td>
          <td>Email</td>
          <td>Country</td>
          <td>Actions</td>
          <td class="text-center" colspan='2'>Operations</td>
        </tr>
    </thead>
    <tbody>
        
        @foreach($customers as $customer)
        <tr>
            <td>{{$customer->firstName}} {{$customer->lastName}}</td>
            <td>{{$customer->email}}</td>
            <td>{{$customer->country->name}}</td>
            <td>
                <a href="{{ route('customer.edit',$customer->id)}}" class="btn btn-outline-primary">
                    <i class="fas fa-edit"></i>
                    Edit
                </a>
            </td>
            <td class="text-center"> 
                <a href="{{ route('account.depositview',$customer->id)}}" class="btn btn-outline-secondary">
                    <i class="fas fa-angle-down text-success"></i>
                    Deposit
                </a>
            </td>
            <td class="text-center">
                <a href="{{ route('account.widrawview',$customer->id)}}" class="btn btn-outline-secondary">
                    <i class="fas fa-angle-up text-danger"></i>
                    Widraw
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
  @else
  <div>No customers to show</div>
 @endif
<div>
</div>
@endsection