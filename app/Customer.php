<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];

    public function account()
    {
        return $this->hasOne('App\Account');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }
}
