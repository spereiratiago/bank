# Bank exercice

This project is made with Laravel framework v7


## requirements

```bash

-Composer

-Wamp server (PHP >= 7.2.5)

PHP extensions:

>BCMath PHP Extension
>Ctype PHP Extension
>Fileinfo PHP extension
>JSON PHP Extension
>Mbstring PHP Extension
>OpenSSL PHP Extension
>PDO PHP Extension
>Tokenizer PHP Extension
>XML PHP Extension

```
Can also be found here for more details [Link](https://laravel.com/docs/7.x/installation) 


## Install

On the project directory run this commands

```bash
composer install

cp .env.example .env

php artisan key:generate

```
-> Create also an empty database 

-> Modify the .env file with credentials to database and the name

for example:
```bash

....

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=bank
DB_USERNAME=root
DB_PASSWORD=

...
```
Then to create tables and seed them

```bash


php artisan migrate


php artisan db:seed


```
## Usage

```bash
php artisan serve

```

By defaul it is available at [http://localhost:8000/](http://localhost:8000/)