@extends('master.base')

@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-1">Add a customer</h1>
    <div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <form method="post" action="{{ route('customer.store') }}">
        @csrf
        <div class="form-row">
            <div class="form-group">    
                <label for="first_name">First Name:</label>
                <input type="text" class="form-control" name="first_name" required/>
            </div>

            <div class="form-group">
                <label for="last_name">Last Name:</label>
                <input type="text" class="form-control" name="last_name" required/>
            </div>
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" name="email" required/>
        </div>
        
        <div class="form-group">
        <label class="form-check-label" for="country">Country:</label>
            <select name="country" class='form-control' required>
                <option value="" selected disabled>Please select..</option>
                @foreach($countries as $country)
                    <option value="{{$country->id}}">{{$country->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
        <label class="form-check-label" for="gender">Gender:</label>
            <select name="gender" class='form-control' required>
                <option value="" selected disabled>Please select..</option>
                @foreach($genders as $gender)
                    <option value="{{$gender->id}}">{{$gender->name}}</option>
                @endforeach
            </select> 
        </div> 

        <button type="submit" class="btn btn-primary-outline">Add contact</button>
        <button type="button" class="btn btn-danger-outline" onclick="javascript:history.go(-1);">cancel</button>
    </form>
  </div>
</div>
</div>
@endsection