<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Customer;
use App\Account;

class AccountOperationsTest extends TestCase
{
    use RefreshDatabase;

    /**
     *  test
     *
     * @return void
     */
    public function can_widraw_from_account()
    {
        $response = $this->post('/account/1/widraw', [
            amount    =>  1,
        ]);

        $response->assertOk();
    }

    public function canot_widraw_to_negative_amount()
    {
        $response = $this->post('/account/1/widraw', [
            amount    =>  10000,
        ]);

        $response->assertError();
    }

    public function can_deposit_amount()
    {
        $response = $this->post('/account/1/widraw', [
            amount    =>  10,
        ]);

        $response->assertOk();
    }
}
