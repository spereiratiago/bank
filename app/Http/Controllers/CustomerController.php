<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Account;
use App\Gender;
use App\Country;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('country')->get();
        return view('customer.list', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $genders = Gender::all();

        return view('customer.create', compact('countries','genders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|unique:customers'
        ]);

        $customer = new Customer([
            'firstName' => $request->get('first_name'),
            'lastName' => $request->get('last_name'),
            'email' => $request->get('email'),
            'gender_id' => $request->get('gender'),
            'country_id' => $request->get('country'),
            'bonus' => rand(5,20)
        ]);
        
        $customer->save();

        //needs separation from this controller
        $account = new Account(['amount' => 0.00]);

        $customer->account()->save($account);

        return redirect('/customer')->with('success', 'Customer created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        $countries = Country::all();
        $genders = Gender::all();

        return view('customer.edit', compact('customer','countries','genders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Customer $customer, Request $request)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required'
        ]);

        $customer->firstName =  $request->get('first_name');
        $customer->lastName = $request->get('last_name');
        $customer->email = $request->get('email');
        $customer->gender_id = $request->get('gender');
        $customer->country_id = $request->get('country');
        $customer->save();

        return redirect('/customer')->with('success', 'Costumer updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //not implemented
    }
}
