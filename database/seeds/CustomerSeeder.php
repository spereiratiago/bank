<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'firstName' => Str::random(5),
            'lastName' => Str::random(5),
            'email' => Str::random(10).'@gmail.com',
            'country_id' => 10,
            'gender_id' => 1,
            'bonus' => 5,
        ]);
    }
}
