<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Customer;

class CustomerRegistrationTest extends TestCase
{
    use RefreshDatabase;

    /**
     *  test
     *
     * @return void
     */
    public function can_register_customer()
    {
        $response = $this->post('/customer', [
            firstName   => 'John',
            lastName    =>  'Awesome',
            gender  =>  'M',
            country =>  'PT',
            email   =>  'john.awesome@gmail.com',
        ]);

        $response->assertOk();
        $response->assertCount(1, Customer::all());
    }
}
