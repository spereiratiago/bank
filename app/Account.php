<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $guarded = [];
    
    public function account()
    {
        return $this->hasOne('App\Customer');
    }

}
