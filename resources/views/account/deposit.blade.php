@extends('master.base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
  @if(session()->get('errors'))
    <div class="alert alert-danger">
      {{ session()->get('errors') }}  
    </div>
  @endif

        <h1 class="display-1">Deposit amount</h1>
        <h3>current ammount: {{ $account->amount }} <i class="fas fa-euro-sign"></i></h3>
        
        <form method="post" action="{{ route('account.deposit', $account->id) }}">
            @csrf
            <div class="form-row">
            <div class="form-group">    
                <label for="first_name">Insert amount:</label>
                <input type="number" min="1" max="500" class="form-control" name="amount"  required/>
            </div>
            <button type="submit" class="btn btn-primary-outline">Deposit</button>
            <button type="button" class="btn btn-danger-outline" onclick="javascript:history.go(-1);">cancel</button>

        </form>
    </div>
</div>
@endsection