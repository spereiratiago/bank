<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Fantastic Internal Bank</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" rel="stylesheet">
</head>
<body>
  <div class="container">
  <nav class="navbar navbar-light">
  <a class="navbar-brand text-center" href="{{ route('customer.index')}}">
  
  <h2> <i class="fas fa-university"></i> Internal Bank<h2>
    </a>
    
    <!-- // menu // -->
  </nav>
  <hr>
    @yield('main')
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js">
</body>
</html>
