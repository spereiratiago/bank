<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{

    /**
     * Deposit determined amount.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function depositView(Account $account)
    {
        return view('account.deposit', compact('account'));
    }

    /**
     * Deposit determined amount.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deposit(Account $account, Request $request)
    {
        $account->amount =  $account->amount + $request->get('amount');
        $account->save();

        return redirect()->back()->with('success', 'Success!');
    }

    /**
     * Deposit determined amount.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function widrawView(Account $account)
    {
        return view('account.widraw', compact('account'));
    }

    /**
     * widraw determined ammount.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function widraw(Account $account, Request $request)
    {
        if ($account->amount - $request->get('amount') >= 0)
        {
            $account->amount =  $account->amount - $request->get('amount');
            $account->save();
            return redirect()->back()->with('success', 'Success!');
        }
        
        return redirect()->back()->with('errors', 'There was an error with this amount to widraw please try again!');
    }
}
